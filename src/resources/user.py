from flask_restful import reqparse, Resource
from models.user import UserModel


# userRegister class
class UserRegister(Resource):
    # class level
    parser = reqparse.RequestParser()
    parser.add_argument(
        "username", type=str, required=True, help="username field cannot be blank"
    )
    parser.add_argument(
        "password", type=str, required=True, help="password field cannot be blank"
    )

    def post(self):
        data = UserRegister.parser.parse_args()

        if UserModel.find_by_username(data["username"]):
            return {"message": "username already exists"}, 400

        user = UserModel(data["username"], data["password"])
        user.save_to_db()

        return {"message": "user created successfully"}, 201
