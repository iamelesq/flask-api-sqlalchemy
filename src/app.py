# package imports
from flask import Flask
from flask_restful import Api
from flask_jwt import JWT

# local imports
from security import authenticate, identity
from resources.user import UserRegister
from resources.item import Item, ItemList
from resources.store import Store, StoreList
from db import db

# app
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = "ed"
api = Api(app)

# checks before the first request and will create the db
# and necessary tables if required.
@app.before_first_request
def create_tables():
    db.create_all()


# creates a new endpoint /auth which takes the passed username
# and password and passes it to the autheticate function.
# Autheticate locates a user or sends None and where a usr is
# located the validity of the password is checked.
jwt = JWT(app=app, authentication_handler=authenticate, identity_handler=identity)


api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")
api.add_resource(Store, "/store/<string:name>")
api.add_resource(StoreList, "/stores")
api.add_resource(UserRegister, "/register")


if __name__ == "__main__":
    db.init_app(app)
    app.run(port=5000, debug=True)
