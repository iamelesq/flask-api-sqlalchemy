# flask API demo

A demo project using flask to create a basic API.

### using

- flask
- flask_restful
- SQLAlchemy
- SQLite

### feature missed

- https
- deployment
- non trivial DB engine
